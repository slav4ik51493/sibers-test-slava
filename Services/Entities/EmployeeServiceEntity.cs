﻿
using System.Collections.Generic;

namespace Services.Entities
{
    public class EmployeeServiceEntity
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }

    }
}
