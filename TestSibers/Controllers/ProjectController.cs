﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Services.Entities;
using Services.Services;
using TestSibers.Models;
using WebGrease.Css.Extensions;

namespace TestSibers.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;
        public ProjectController(IProjectService projectService, IEmployeeService employeeService)
        {
            _projectService = projectService;
            _employeeService = employeeService;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProjectServiceEntity, ProjectModel>();
                cfg.CreateMap<ProjectModel, ProjectServiceEntity>();
            });
            _mapper = config.CreateMapper();
        }

        public ActionResult Index(string searchFilter, DateTime? startDate, DateTime? endDate, int? fromPrior, int? toPrior, SortProject sort = 0)
        {
            var projects = _projectService.GetAllProject(searchFilter, sort);

            if (startDate != null && endDate != null)
                projects = projects.Where(c => c.StartDate >= startDate && c.EndDate <= endDate).ToList();

            if (fromPrior != null && toPrior != null)
                projects = projects.Where(c => c.Priority >= fromPrior && c.Priority <= toPrior).ToList();

            return View(_mapper.Map<ICollection<ProjectServiceEntity>, ICollection<ProjectModel>>(projects.ToList()));

        }

        public ActionResult NewProject(ProjectModel project)
        {
            return View(project);
        }

        public ActionResult Edit(int id, ProjectModel project)
        {

            return View(_mapper.Map<ProjectServiceEntity, ProjectModel>(_projectService.GetById(id)));
        }

        public ActionResult FullProject(int? id)
        {
            if (id == null)
                return HttpNotFound();
            var project = _mapper.Map<ProjectServiceEntity, ProjectModel>(_projectService.GetById(id.Value));
            ViewBag.employees = _mapper.Map<ICollection<EmployeeServiceEntity>, ICollection<EmployeeModel>>(_projectService.GetAllEmployees(id.Value));
            if (project != null)
                return View(project);
            return null;
        }

        [HttpPost]
        public string SearchManager(string str)
        {
            if (Request.IsAjaxRequest())
                if (str.Length == 0)
                {
                    var employees = _employeeService.GetAll().Take(10);
                    var request = new StringBuilder();
                    employees.ForEach(c => request.Append("<li>" + c.Id + " " + c.Surname + " " + c.FirstName + " " + c.Patronymic + "</li>"));
                    return request.ToString();
                }
                else
                {
                    var employees = _employeeService.GetAll().ToList().Where(c =>
                    {
                        if (c.FirstName.ToLower().Contains(str.ToLower()) || c.Surname.ToLower().Contains(str.ToLower()) || c.Patronymic.ToLower().Contains(str.ToLower()))
                            return true;
                        return false;
                    });
                    var request = new StringBuilder();
                    employees.Take(10).ForEach(c => request.Append("<li>" + c.Id + " " + c.Surname + " " + c.FirstName + " " + c.Patronymic + "</li>"));
                    return request.ToString();
                }
            return null;
        }

        public RedirectResult Create(ProjectModel project)
        {
            _projectService.Add(_mapper.Map<ProjectModel, ProjectServiceEntity>(project));
            return Redirect("~/Project");
        }

        public RedirectResult Delete(int id)
        {
            _projectService.Delete(id);
            return Redirect("~/Project");
        }

        public RedirectResult Bind(int? projectId, int? ProjectManagerId)
        {
            if (projectId != null && ProjectManagerId != null)
                _projectService.Bind(projectId.Value, ProjectManagerId.Value);

            return Redirect($"~/Project/FullProject?id={projectId}");
        }

        public RedirectResult UnBind(int projectId, int employeeId)
        {
            _projectService.UnBind(projectId, employeeId);
            return Redirect($"~/Project/FullProject?id={projectId}");
        }

        public RedirectResult Save(ProjectModel project)
        {
            _projectService.Update(_mapper.Map<ProjectModel, ProjectServiceEntity>(project));
            return Redirect("~/project");
        }
    }
}