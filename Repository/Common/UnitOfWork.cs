﻿using System;
using System.Data.Entity;

namespace Repository.Common
{
    public interface IUnitOfWork
    {
        int Commit();
    }
    internal class UnitOfWork : IUnitOfWork, IDisposable
    {
        private DbContext _dbContext;

        public UnitOfWork(DbContext context)
        {
            _dbContext = context;
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing || _dbContext == null) return;

            _dbContext.Dispose();
            _dbContext = null;
        }
    }
}
