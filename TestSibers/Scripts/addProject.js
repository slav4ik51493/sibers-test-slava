﻿$(function () {


    jQuery('form').submit(function () {
        /*if (parseInt($('#Priority').val()) < 0) {
            alert("Приоритет не может быть меньше 0.");
            return false;
        }*/

        if (Date.parse($('#StartDate').val()) > Date.parse($('#EndDate').val())) {    
            alert("Дата начала проекта не может быть позже, чем дата окончания. Проверьте и попробуйте еще раз.");
	        return false;
        }

        return true;
    });

    //Живой поиск
    $('.who').bind("change keyup input click",
        function () {
            if (this.value.length >= 0) {
                $.ajax({
                    type: 'post',
                    url: "../Project/SearchManager",
                    data: { 'str': this.value },
                    response: 'text',
                    success: function (data) {
                        $(".search_result").html(data).fadeIn();
                    }
                });
            }
        });
    $(".search_result").hover(function () {
        $(".who").blur();
    });

	$("#pickAnother").on("click",
        function () {
	        $("#ProjectManagerId").val("");
            $(".who").val("").removeAttr('disabled', 'enabled');
	        $("#pickAnother").fadeOut();
        });

    $(".search_result").on("click",
        "li",
        function () {
            selectedPM = $(this).text();
            $(".who").val(selectedPM).attr('disabled', 'disabled');
	        $("#pickAnother").fadeIn();
            $("#ProjectManagerId").val(parseInt(selectedPM.split(' ')[0]));
            $(".search_result").fadeOut();
        });

})